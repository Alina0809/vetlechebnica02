FROM node:14.15.3

RUN mkdir /app
WORKDIR /app

COPY package.json /app/package.json
COPY . /app
COPY .env.prod /app/.env
RUN npm install

EXPOSE 8084

CMD node server.js
