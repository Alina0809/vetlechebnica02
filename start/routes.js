'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')


Route.on('/').render('home')
Route.on('/about').render('about')
Route.get('/gallery', 'SectionsController.galleryGet')
Route.on('/about/history').render('history')
// Route.on('/about/workers').render('workers')
Route.get('/about/workers/', 'SectionsController.workers')

Route.on('/services/sterilizacziya/').render('services/sterilizacziya')
Route.on('/services/vakczinacziya/').render('services/vakczinacziya')
Route.on('/services/udalenie-zubnyx-kamnej/').render('services/udalenie-zubnyx-kamnej')
Route.on('/services/operativnaya-xirurgiya/').render('services/operativnaya-xirurgiya')
Route.on('/services/obsledovaniya-zhivotnyx/').render('services/obsledovaniya-zhivotnyx')

Route.on('/services').render('services/index')
Route.on('/services/desinfektion').render('services/desinfektion')
// Route.on('/services/apteka').render('shop/index')
Route.get('/services/apteka/', 'SectionsController.apteka')

Route.post('/admin/users/login', 'UserController.login')
Route.get('/admin/users/logout', 'UserController.logout')
Route.post('/admin/users/register', 'UserController.register')

Route.get('/admin/users/', 'UserController.users')
Route.get('/admin/users/:id', 'UserController.user')
Route.get('/admin/users/:id/delete', 'UserController.delete')
Route.post('/admin/users/:id', 'UserController.edit')

Route.get('/admin/sections/:id/delete', 'SectionsController.delete')
Route.get('/admin/section/:sectionid/:id/delete', 'SectionsController.deleteItem')
Route.get('/about/sections/', 'SectionsController.get')

Route.post('/admin/gallery/', 'SectionsController.gallerySet')
Route.get('/admin/gallery/', 'SectionsController.gallery')
Route.get('/admin/gallery/:gallery_id/delete', 'SectionsController.galleryDelete')

Route.post('/admin/apteka/', 'SectionsController.aptekaSet')
Route.get('/admin/apteka/', 'SectionsController.aptekaGet')
Route.get('/admin/apteka/:product_id/edit', 'SectionsController.product')
Route.post('/admin/apteka/:product_id/edit', 'SectionsController.productSave')

Route.post('/admin/orders/', 'SectionsController.ordersSet')
Route.get('/admin/orders/', 'SectionsController.ordersGet')
Route.get('/admin/orders/:order_id/confirm', 'SectionsController.ordersConfirm')
Route.get('/admin/orders/:order_id/delete', 'SectionsController.ordersDelete')

Route.post('/admin/worker/:worker_id/', 'SectionsController.workerSave')
Route.get('/admin/worker/:worker_id/', 'SectionsController.worker')

Route.post('/admin/workers/', 'SectionsController.workersSet')
Route.get('/admin/workers/', 'SectionsController.workersGet')
Route.get('/admin/workers/:order_id/delete', 'SectionsController.workersDelete')

Route.get('/admin/sections/', 'SectionsController.adminGet')
Route.post('/admin/sections/', 'SectionsController.addSection')
Route.get('/admin/sections/:id', 'SectionsController.sectionItems')
Route.post('/admin/sections/:id', 'SectionsController.sectionCreate')
