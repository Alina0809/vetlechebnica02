'use strict'


/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Section extends Model {
  sections () {
    return this.hasMany('App/Models/SectionItem')
  }
}

module.exports = Section
