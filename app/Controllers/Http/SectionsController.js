'use strict'

const Section = use('App/Models/Section')
const Order = use('App/Models/Order')
const Worker = use('App/Models/Worker')
const Gallery = use('App/Models/Gallery')
const Product = use('App/Models/Product')
const SectionItem = use('App/Models/SectionItem')

class SectionsController {
  async ordersSet({ request, auth, response, view }) {
    try {

      const name = request.input("name")
      const phone = request.input("phone")
      const msg = request.input("msg")

      const order = new Order();
      order.name = name;
      order.phone = phone;
      order.msg = msg;
      await order.save();
      response.redirect('/')
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }
  async workersSet({ request, auth, response, view }) {
    try {

      const name = request.input("name")
      const birthday = request.input("birthday")
      const position = request.input("position")

      const worker = new Worker();
      worker.name = name;
      worker.birthday = birthday;
      worker.position = position;
      await worker.save();
      response.redirect('/admin/workers/')
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }
  async gallerySet({ request, auth, response, view }) {
    try {

      const image = request.input("image")

      const gallery = new Gallery();
      gallery.image = image;
      await gallery.save();
      response.redirect('/admin/gallery/')
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }
  async aptekaSet({ request, auth, response, view }) {
    try {

      const name = request.input("name")
      const count = request.input("count")
      const price = request.input("price")
      const desc = request.input("desc")
      const image = request.input("image")

      const product = new Product();
      product.name = name;
      product.count = count;
      product.price = price;
      product.desc = desc;
      product.image = image;
      await product.save();
      response.redirect('/admin/apteka/')
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }
  async ordersGet({ request, auth, response, view }) {
    try {
      await auth.check();
    } catch (e) {
      return response.redirect('/')
    }
    try {
      const orders = await Order.all()

      return view.render('admin.orders', {
        orders: orders.toJSON(),
      })
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }
  async workersGet({ request, auth, response, view }) {
    try {
      await auth.check();
    } catch (e) {
      return response.redirect('/')
    }
    try {
      const workers = await Worker.all()

      return view.render('admin.workers', {
        workers: workers.toJSON(),
      })
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }
  async aptekaGet({ request, auth, response, view }) {
    try {
      await auth.check();
    } catch (e) {
      return response.redirect('/')
    }
    try {
      const products = await Product.all()

      return view.render('admin.apteka', {
        products: products.toJSON(),
      })
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }
  async apteka({ request, auth, response, view }) {
    try {
      await auth.check();
    } catch (e) {
      return response.redirect('/')
    }
    try {
      const products = await Product.all()

      return view.render('shop.index', {
        products: products.toJSON(),
      })
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }
  async gallery({ request, auth, response, view }) {
    try {
      await auth.check();
    } catch (e) {
      return response.redirect('/')
    }
    try {
      const gallery = await Gallery.all()

      return view.render('admin.gallery', {
        gallery: gallery.toJSON(),
      })
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }
  async galleryGet({ request, auth, response, view }) {
    try {
      await auth.check();
    } catch (e) {
      return response.redirect('/')
    }
    try {
      const gallery = await Gallery.all()

      return view.render('gallery', {
        gallery: gallery.toJSON(),
      })
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }
  async workers({ request, auth, response, view }) {
    try {
      await auth.check();
    } catch (e) {
      return response.redirect('/')
    }
    try {
      const workers = await Worker.all()

      return view.render('workers', {
        workers: workers.toJSON(),
      })
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }
  async ordersConfirm({ request, auth, response, view, params }) {
    try {
      await auth.check();
    } catch (e) {
      return response.redirect('/')
    }
    try {
      const order = await Order.find(params.order_id)

      order.status = 'ok';
      await order.save();
      return response.redirect('/admin/orders/')
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }
  async ordersDelete({ request, auth, response, view, params }) {
    try {
      await auth.check();
    } catch (e) {
      return response.redirect('/')
    }
    try {
      const order = await Order.find(params.order_id)
      await order.delete();
      return response.redirect('/admin/orders/')
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }
  async galleryDelete({ request, auth, response, view, params }) {
    try {
      await auth.check();
    } catch (e) {
      return response.redirect('/')
    }
    try {
      const gallery = await Gallery.find(params.gallery_id)
      await gallery.delete();
      return response.redirect('/admin/gallery/')
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }
  async workersDelete({ request, auth, response, view, params }) {
    try {
      await auth.check();
    } catch (e) {
      return response.redirect('/')
    }
    try {
      const worker = await Worker.find(params.order_id)
      await worker.delete();
      return response.redirect('/admin/workers/')
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }
  async workerSave({ request, auth, response, view, params }) {
    try {
      await auth.check();
    } catch (e) {
      return response.redirect('/')
    }
    try {
      const name = request.input("name")
      const birthday = request.input("birthday")
      const position = request.input("position")

      const worker = await Worker.find(params.worker_id)

      worker.name = name;
      worker.birthday = birthday;
      worker.position = position;

      await worker.save();
      return response.redirect('/admin/workers/')
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }
  async productSave({ request, auth, response, view, params }) {
    try {
      await auth.check();
    } catch (e) {
      return response.redirect('/')
    }
    try {
      const name = request.input("name")
      const count = request.input("count")
      const price = request.input("price")
      const image = request.input("image")
      const desc = request.input("desc")

      const product = await Product.find(params.product_id)

      product.name = name;
      product.count = count;
      product.price = price;
      product.image = image;
      product.desc = desc;

      await product.save();
      return response.redirect('/admin/apteka/')
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }
  async worker({ request, auth, response, view, params }) {
    try {
      await auth.check();
    } catch (e) {
      return response.redirect('/')
    }
    try {
      const worker = await Worker.find(params.worker_id)

      return view.render('admin.worker', {
        worker
      })
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }
  async product({ request, auth, response, view, params }) {
    try {
      await auth.check();
    } catch (e) {
      return response.redirect('/')
    }
    try {
      const product = await Product.find(params.product_id)

      return view.render('admin.product', {
        product: product.toJSON()
      })
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }
  async get({ request, auth, response, view }) {
    try {
      await auth.check();
    } catch (e) {
      return response.redirect('/')
    }
    try {



      const sections = await Section
        .query()
        .with('sections')
        .fetch()

      console.log((sections.toJSON())[0].sections)

      return view.render('sections', {
        sections: sections.toJSON(),
        sectionsIndex: () => {
          if (!this.count) {
            this.count = 0
          }
          this.count++;
          return this.count;
        },
      })
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }
  async adminGet({ request, auth, response, view }) {
    try {
      await auth.check();
    } catch (e) {
      return response.redirect('/')
    }
    try {

      const sections = await Section
        .query()
        .with('sections')
        .fetch()


      return view.render('admin.sections', {
        sections: sections.toJSON(),
        sectionsIndex: () => {
          if (!this.count) {
            this.count = 0
          }
          this.count++;
          return this.count;
        },
      })
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }

  async sectionItems({ request, auth, response, view, params }) {
    try {
      await auth.check();
    } catch (e) {
      return response.redirect('/')
    }
    try {

      const section = await Section.find(params.id);
      const sections = await section.sections().fetch();


      return view.render('admin.section', {
        sections: sections.toJSON(),
        section: section.toJSON(),
        sectionsIndex: () => {
          if (!this.count) {
            this.count = 0
          }
          this.count++;
          return this.count;
        },
      })
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }

  async addSection({ request, auth, response, view }) {
    try {
      await auth.check();
    } catch (e) {
      return response.redirect('/')
    }
    try {
      const name = request.input("name")
      const expert = request.input("expert")

      const section = new Section();
      section.expert = expert;
      section.name = name;
      await section.save();

      const sections = await Section
        .query()
        .with('sections')
        .fetch()


      return view.render('admin.sections', {
        sections: sections.toJSON(),
        sectionsIndex: () => {
          if (!this.count) {
            this.count = 0
          }
          this.count++;
          return this.count;
        },
      })
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }
  async sectionCreate({ request, auth, response, view, params }) {
    try {
      await auth.check();
    } catch (e) {
      return response.redirect('/')
    }
    try {
      const name = request.input("name")
      const _method = request.input("_method") || ''
      const expert = request.input("expert") || ''
      let section = await Section.find(params.id);
      if (_method === 'put') {
        section.expert = expert;
        section.name = name;
        await section.save()
      } else {
        let sectionItem = new SectionItem();
        sectionItem.expert = expert;
        sectionItem.name = name;
        await section.sections().save(sectionItem)
      }

      const sections = await section.sections().fetch();

      return view.render('admin.section', {
        sections: sections.toJSON(),
        section: section.toJSON(),
        sectionsIndex: () => {
          if (!this.count) {
            this.count = 0
          }
          this.count++;
          return this.count;
        },
      })
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }

  async delete({ request, auth, response, view,params }) {
    try {
      await auth.check();
    } catch (e) {
      return response.redirect('/')
    }
    try {
      const section = await Section.find(params.id);

      await section.delete();

      response.redirect('/admin/sections/')
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }

  async deleteItem({ request, auth, response, view,params }) {
    try {
      await auth.check();
    } catch (e) {
      return response.redirect('/')
    }
    try {
      const section = await SectionItem.find(params.id);

      await section.delete();

      response.redirect('/admin/sections/' + params.sectionid)
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }
}

module.exports = SectionsController
