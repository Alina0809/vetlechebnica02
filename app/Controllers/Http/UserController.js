'use strict'

const User = use('App/Models/User')

class UserController {
  async register({ request, auth, response }) {
    try {
      const email = request.input("email")
      const password = request.input("password")
      const fname = request.input("fname") || '';
      const lname = request.input("lname") || '';
      const userExists = await User.findBy('email', email)
      if (userExists) {
        return response.status(400).send({
          status: 'error',
          message: 'Пользователь с таким email существует'
        })
      }
      let user = new User()
      user.email = email
      user.password = password
      user.fname = fname
      user.lname = lname
      let success = await user.save()
      return response.status(201).json({
        status: 'ok',
        message: 'User is registered',
        success: success,
        UserID: user['_id']
      })
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        debug_error: error.message,
      })
    }
  }
  async login({ request, auth, response }) {
    const { email, password } = request.all()
    try {
      console.log(email, password)
      let token = await auth.attempt(email, password)
      return response.status(200).json({
        status: 'ok',
        message: 'Logged in',
        token: token
      })
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }

  async logout({ request, auth, response }) {
    try {
      await auth.logout()
      response.status(200).json({
        status: true
      })
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }
  async users({ request, auth, response, view }) {
    try {
      await auth.check();
    } catch (e) {
      return response.redirect('/')
    }
    try {
      const users = await User.all()
      console.log(users)
      return view.render('admin.users', {
        users: users.toJSON()
      })
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }

  async user({ request, auth, response, view,params }) {
    try {
      await auth.check();
    } catch (e) {
      return response.redirect('/')
    }
    try {
      const user = await User.find(params.id)
      return view.render('admin.user', {
        user: user.toJSON()
      })
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }

  async edit({ request, auth, response, view,params }) {
    try {
      await auth.check();
    } catch (e) {
      return response.redirect('/')
    }
    try {
      const user = await User.find(params.id);

      const fname = request.input("fname")
      const password = request.input("password")
      if (password) {
        user.password = password;
      }
      user.fname = fname;
      await user.save();

      return view.render('admin.user', {
        user: user.toJSON()
      })
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }

  async delete({ request, auth, response, view,params }) {
    try {
      await auth.check();
    } catch (e) {
      return response.redirect('/')
    }
    try {
      const user = await User.find(params.id);

      await user.delete();

      response.redirect('/admin/users/')
    } catch (error) {
      console.log(error.message)
      response.status(403).json({
        status: 'error',
        message: error.message
      })
    }
  }
}

module.exports = UserController
