docker rmi $(docker images --filter "dangling=true" -q --no-trunc)

docker build -t vetlechebnica-web -f Dockerfile .;

docker run --name "vetlechebnica-mongo" \
           -v /data/mongodb/db:/data/db \
           -p 27017:27017 \
           -d mongo

docker start vetlechebnica-mongo || false;

docker rm -f "vetlechebnica-web";

docker run \
    --name "vetlechebnica-web" \
    -e TZ="Europe/Moscow" \
    -p 8084:8084 \
    -v /app/node_modules \
    -d \
    --link "vetlechebnica-mongo":mongo \
    vetlechebnica-web;
